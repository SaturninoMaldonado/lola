#include "lola_board.h"

#define VERSION "V1.10"

#define IGNORE_PULSE   3000 // time in micros to ignore encoder pulses if faster


unsigned char SPEED_INI_L=255;  // 170
unsigned char SPEED_INI_R=255;  // 100

// Direction of movement
unsigned char dir_right,dir_left;

// Variables to keep each encoder pulse
volatile unsigned int encoderIZQ = 0, encoderDER = 0;

// Variables to obtain robot position and orientation (X, Y Theta)
unsigned int aux_encoderIZQ = 0, aux_encoderDER = 0;
volatile signed int encoder = 0;
//unsigned long pulsesDER=0;
//unsigned long pulsesIZQ=0;

// Auxiliar variables to filter false impulses from encoders
volatile unsigned long auxr=0,auxl=0;
// Auxiliar variables to keep micros() at encoders
unsigned long tr,tl;

// Indicate the PWM that is applied to the motors
int velr = SPEED_INI_R;
int vell = SPEED_INI_L;
int error = 0;
int encoder_ant;


void setup() {
  
  // Add the interrupt lines for encoders
  attachInterrupt(digitalPinToInterrupt(MOT_R_ENC_B_PIN), cuentaDER, FALLING);
  
  attachInterrupt(digitalPinToInterrupt(MOT_L_ENC_B_PIN), cuentaIZQ, FALLING);

  //Battery pin for voltaje measurement
  pinMode(BAT_PIN,         INPUT);

  //Dip switch for configuration
  pinMode(SW1_PIN,  INPUT_PULLUP);
  pinMode(SW2_PIN,  INPUT_PULLUP);
  pinMode(SW3_PIN,  INPUT_PULLUP);
  pinMode(SW4_PIN,  INPUT_PULLUP);
  pinMode(SW5_PIN,  INPUT_PULLUP);
  pinMode(SW6_PIN,  INPUT_PULLUP);
  pinMode(SW7_PIN,  INPUT_PULLUP);
  pinMode(SW8_PIN,  INPUT_PULLUP);

  //Buzzer
  pinMode(BUZZER_PIN, OUTPUT);

  // set all the motor control pins to outputs
  pinMode(MOT_R_PWM_PIN, OUTPUT);
  pinMode(MOT_L_PWM_PIN, OUTPUT);
  
  pinMode(MOT_R_A_PIN, OUTPUT);
  pinMode(MOT_R_B_PIN, OUTPUT);
  pinMode(MOT_L_A_PIN, OUTPUT);
  pinMode(MOT_L_B_PIN, OUTPUT);

  // set encoder pins to inputs
  pinMode(MOT_L_ENC_B_PIN, INPUT);
  pinMode(MOT_R_ENC_B_PIN, INPUT);

  //L RGB LED
  pinMode(L_RED_PIN,      OUTPUT);
  pinMode(L_GRE_PIN,      OUTPUT);
  pinMode(L_BLU_PIN,      OUTPUT);
  
  //R RGB LED
  pinMode(R_RED_PIN,      OUTPUT);
  pinMode(R_GRE_PIN,      OUTPUT);
  pinMode(R_BLU_PIN,      OUTPUT);

  //F Ultrasound sensor
  pinMode(F_US_TRIG,      OUTPUT);
  pinMode(F_US_ECHO,      INPUT);
  //L Ultrasound sensor
  pinMode(L_US_TRIG,      OUTPUT);
  pinMode(L_US_ECHO,      INPUT);
  //R Ultrasound sensor
  pinMode(R_US_TRIG,      OUTPUT);
  pinMode(R_US_ECHO,      INPUT);
  //B Ultrasound sensor
  pinMode(R_US_TRIG,      OUTPUT);
  pinMode(R_US_ECHO,      INPUT);

  
  // set buttons pins
  pinMode(PIN_FORWARD, INPUT_PULLUP);
  pinMode(PIN_BACKWARD, INPUT_PULLUP);
  pinMode(PIN_LEFT, INPUT_PULLUP);
  pinMode(PIN_RIGHT, INPUT_PULLUP);
  
  pinMode(LED, OUTPUT);

  digitalWrite(MOT_R_A_PIN, LOW);
  digitalWrite(MOT_R_B_PIN, LOW);    
  digitalWrite(MOT_L_A_PIN, LOW);
  digitalWrite(MOT_L_B_PIN, LOW);

  analogWrite(MOT_R_PWM_PIN, 0);
  analogWrite(MOT_L_PWM_PIN, 0);

  Serial.begin(38400);      //init the serial port  
  Serial.print("LOLA INI ");
  Serial.println(VERSION);
  Serial.println("Seleccione mediante el SWITCH de la placa un modo de funcionamiento");

}  // End of setup()


//////////////////////////////////////////////////
//  Right encoder interrupt using only one pin
//////////////////////////////////////////////////
void cuentaDER()
{
  tr=micros();
  // if pulse is too fast from previoius is ignored
  if (tr-auxr>IGNORE_PULSE)
  {
    auxr=tr;
    encoderDER++;    //Add one pulse
  }
  
}  // end of cuentaDER

//////////////////////////////////////////////////
//  Left encoder interrupt using only one pin
//////////////////////////////////////////////////
void cuentaIZQ()
{
  tl=micros();
  // if pulse is too fast from previoius is ignored
  if (tl-auxl>IGNORE_PULSE)
  {
    auxl=tl;
    encoderIZQ++;  //Add one pulse
  }
}  // end of cuentaIZQ


//////////////////////////////////////////////////
//  MOVE MOTORS:
//  dir_right (1: foward / 0: backwards)
//  dir_left  (1: foward / 0: backwards)
// 
//
//////////////////////////////////////////////////
void move_motors()
{
  // now turn off motors
  // Adaptation for L298n
  encoderIZQ = 0;
  encoderDER = 0;
  aux_encoderIZQ = 0;
  aux_encoderDER = 0;
  encoder = 0;
  encoder_ant=0;

  if (dir_right==1)
  {
    // Right motor
    digitalWrite(MOT_R_A_PIN, LOW);
    digitalWrite(MOT_R_B_PIN, HIGH);
  }
  else
  {
    // Right motor
    digitalWrite(MOT_R_A_PIN, HIGH);
    digitalWrite(MOT_R_B_PIN, LOW);    
  }
  
  if (dir_left==1)
  {
    // Left motor
    digitalWrite(MOT_L_A_PIN, LOW);
    digitalWrite(MOT_L_B_PIN, HIGH);
  }
  else
  {
    // Left motor
    digitalWrite(MOT_L_A_PIN, HIGH);
    digitalWrite(MOT_L_B_PIN, LOW);   
  }
  velr=SPEED_INI_R;
  vell=SPEED_INI_L;

  // If any speed is zero breaking mode is activated for that wheel
  if (SPEED_INI_R==0)
  {
    digitalWrite(MOT_R_A_PIN, LOW);
    digitalWrite(MOT_R_B_PIN, LOW);        
    analogWrite(MOT_R_PWM_PIN, 255);
  }
  else
    analogWrite(MOT_R_PWM_PIN, velr);
    
  if (SPEED_INI_L==0)
  {
    digitalWrite(MOT_L_A_PIN, LOW);
    digitalWrite(MOT_L_B_PIN, LOW);        
    analogWrite(MOT_L_PWM_PIN, 255);
  }
  else
    analogWrite(MOT_L_PWM_PIN, vell);
  
}  // end move_motors


//////////////////////////////////////////////////
//  STOP_MOTORS
//////////////////////////////////////////////////
void stop_motors()
{
  
  // now turn off motors
  digitalWrite(MOT_R_A_PIN, LOW);
  digitalWrite(MOT_R_B_PIN, LOW);
  digitalWrite(MOT_L_A_PIN, LOW);
  digitalWrite(MOT_L_B_PIN, LOW);
  analogWrite(MOT_R_PWM_PIN, 255);
  analogWrite(MOT_L_PWM_PIN, 255);
}  // end stop_motors


//////////////////////////////////////////////////
//  SPEED_NORMALIZATION
//
//  Speeds are normalized in order to work 
//  at maximum speed give as SPEED_INI_X
//
//////////////////////////////////////////////////
void speed_normalization()
{
    if (velr>vell)
    {
      vell -= (velr-SPEED_INI_R);
      velr = SPEED_INI_R;
      if (vell<0)
        vell=0;          
    }
    else
    {
      velr -= (vell-SPEED_INI_L);
      vell = SPEED_INI_L;
      if (velr<0)
        velr = 0;
    }
} // end of speed_normalization


void loop() {
  // put your main code here, to run repeatedly:

 
  
  if(digitalRead(SW1_PIN)==LOW)
   {
  
       Serial.println("MODO SRE_BOTONES");
       SRE_Botones();
   }
   else if(digitalRead(SW2_PIN)==LOW)
   { 
       Serial.println("MODO SRE_Joystick");   
       SRE_Joystick();    
   }
  else if(digitalRead(SW3_PIN)==LOW)
   {
       Serial.println("MODO ROBOT");
       Lola();
   }   
  else if(digitalRead(SW4_PIN)==LOW)
   {
       Serial.println("MODO LOLA_VALIDATOR");
       print_message();
       Lola_Validate();
   }
}
