#include <math.h>

//Indicate the analog inputs 
const int X_Pin = A14;
const int Y_Pin = A15;

const float pi = 3.1416;

int cizq[]={75,78,81,84,87,90,94,97,100,103,106,109,113,116,119,122,118,112,105,99,93,86,80,74,-63,-69,-76,-82,-88,-95,-101,-107,-111,-111,-112,-113,-113,-114,-115,-115,-116,-117,-117,-118,-118,-119,-120,-120,-118,-114,-110,-106,-102,-99,-95,-91,48,52,55,59,63,67,71,74};
int cdch[]={-100,-94,-88,-81,-75,-69,-62,-56,78,84,90,97,103,110,116,122,119,117,115,112,110,108,106,103,101,99,97,95,92,90,88,86,83,81,79,77,75,72,70,68,-84,-89,-94,-99,-104,-109,-114,-119,-119,-117,-115,-113,-111,-110,-108,-106,-104,-102,-100,-98,-96,-94,-92,-91};

int X_Value = 0;
int Y_Value = 0;

float modulo=0;
double angle=0;
int indice=0;


void read_joystick(){

  X_Value=analogRead(X_Pin)-510;
  Y_Value=analogRead(Y_Pin)-510;

  if ((X_Value < 70)&& (X_Value>-70)){
    X_Value=0;
  }
  if ((Y_Value < 70)&& (Y_Value>-70)){
    Y_Value=0;
  }
  
  modulo=sqrt(pow(X_Value,2)+pow(Y_Value,2));
  angle=atan2(Y_Value,X_Value);

  if (angle<0)
      angle=angle+2*M_PI;

  indice= floor(64*angle/(2*M_PI));

//  vell = 255/100*cizq[indice]*modulo/128;
//  velr = 255/100*cdch[indice]*modulo/128;
  
 SPEED_INI_L = abs(255/100*cizq[indice]*modulo/128);
 SPEED_INI_R = abs(255/100*cdch[indice]*modulo/128);
  
}

void dep_Joystick(){

   Serial.print("X_VALUE: ");
   Serial.print(X_Value);
   Serial.print("  Y_VALUE: ");
   Serial.print(Y_Value);
   Serial.print(" Motor_izquierdo: ");
   Serial.print(vell);
   Serial.print("  Motor_derecho ");
   Serial.println(velr);
   Serial.print("  indice: ");
   Serial.print(indice);
   Serial.print("  modulo: ");
   Serial.print(modulo);
   Serial.print("  angulo: ");
   Serial.println(angle);

   delay(10);
  
}

void SRE_Joystick(){

  while(1){

    if(digitalRead(SW2_PIN)==HIGH)
    {
          Serial.println("Seleccione mediante el SWITCH de la placa un modo de funcionamiento");
          break;
    }
    read_joystick();

    if(cizq[indice]>0)
      dir_left=1;
    else 
      dir_left=0; 

    if(cdch[indice]>0)
      dir_right=1;
    else 
      dir_right=0;

    move_motors();


    dep_Joystick();
   

    delay(10); 

  }


}
