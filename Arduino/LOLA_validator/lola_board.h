//Battery pin for voltaje measurement
  #define BAT_PIN A0

//Dip switch for configuration
  #define SW1_PIN A1
  #define SW2_PIN A2
  #define SW3_PIN A3
  #define SW4_PIN A4
  #define SW5_PIN A5
  #define SW6_PIN A6
  #define SW7_PIN A7
  #define SW8_PIN A8

//Rear electronics box
  #define REAR_LED_PIN 12
  #define REAR_SW_PIN 9

//Buzzer
  #define BUZZER_PIN 13

//L Motor
  #define MOT_L_PWM_PIN 11
  #define MOT_L_A_PIN 26
  #define MOT_L_B_PIN 22
  #define MOT_L_ENC_A_PIN 19
  #define MOT_L_ENC_B_PIN 18

//R Motor
  #define MOT_R_PWM_PIN 10
  #define MOT_R_A_PIN 28
  #define MOT_R_B_PIN 24
  #define MOT_R_ENC_A_PIN 21
  #define MOT_R_ENC_B_PIN 20

//L RGB LED
  #define L_RED_PIN 5
  #define L_GRE_PIN 2
  #define L_BLU_PIN 3
  
//R RGB LED
  #define R_RED_PIN 7
  #define R_GRE_PIN 6
  #define R_BLU_PIN 8

//AUX RGB LED
  #define AUX_RED_PIN 46
  #define AUX_GRE_PIN 45
  #define AUX_BLU_PIN 44

//F FALL/BUMPER SENSOR
  #define F_FALL_PIN  31
  #define F_BUMP_PIN  33

//L FALL/BUMPER SENSOR
  #define L_FALL_PIN  23
  #define L_BUMP_PIN  25

//R FALL/BUMPER SENSOR
  #define R_FALL_PIN  27
  #define R_BUMP_PIN  29
  
//B FALL/BUMPER SENSOR
  #define B_FALL_PIN  32
  #define B_BUMP_PIN  30

//Front Ultrasound sensor
  #define F_US_ECHO 47
  #define F_US_TRIG 49

//Left Ultrasound sensor
  #define L_US_ECHO 39
  #define L_US_TRIG 41

//Rigth Ultrasound sensor
  #define R_US_ECHO 51
  #define R_US_TRIG 53

//BACK Ultrasound sensor
  #define B_US_ECHO 35
  #define B_US_TRIG 37


