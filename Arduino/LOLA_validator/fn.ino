#include "lola_board.h"

void lola_validate(){
  char key; //This will hold the recibed byte
  unsigned long timeout;

  //Get a byte from the serial port. Keep reading, untill we recive something meaningfull.
  while (1){
    key = Serial.read();
    if (key != -1 & key != '\n' & key != '\r')
      break;
  }
  //Clear the recive buffer, it migth contain a \n o \r
  delay(100);
  while (Serial.available() )
    Serial.read();

  //main test routine
  switch(key){
    case '1':
      Serial.println("Battery ADC Test");
      while (!Serial.available() ){
        Serial.print("RAW Battery measurement:\t");
        Serial.println(analogRead(BAT_PIN) );
        delay(500);
      }
      break;

    case '2':
      Serial.println("DIP Switches TEST");
      while (!Serial.available() ){
        Serial.print("Switch status:\t");
        Serial.print(digitalRead(SW1_PIN) );
        Serial.print(digitalRead(SW2_PIN) );
        Serial.print(digitalRead(SW3_PIN) );
        Serial.print(digitalRead(SW4_PIN) );
        Serial.print(digitalRead(SW5_PIN) );
        Serial.print(digitalRead(SW6_PIN) );
        Serial.print(digitalRead(SW7_PIN) );
        Serial.println(digitalRead(SW8_PIN) );
        delay(500);
      }
      break;
    case '3':
      Serial.println("Rear LED & SW test");
      while (!Serial.available() ){
        digitalWrite(REAR_LED_PIN, HIGH);
        Serial.print("Switch status:\t");
        Serial.println(digitalRead(REAR_SW_PIN) );
        delay(500);
        digitalWrite(REAR_LED_PIN, LOW);
        Serial.print("Switch status:\t");
        Serial.println(digitalRead(REAR_SW_PIN) );
        delay(500);
      }
      break;
    case '4':
      Serial.println("BUZZER Test");
      Serial.println("Buzzer Test. The buzer will sonnd every 500ms for 500ms");
      Serial.println("Press any key to abort...");
      while (!Serial.available() ){
        digitalWrite(BUZZER_PIN, HIGH);
        delay(500);
        digitalWrite(BUZZER_PIN, LOW);
        delay(500);
      }
      break;
    case '5':
      Serial.println("TEST de motores");
      Serial.println("Puedes pulsar una tecla en cualquier momento. El test acabrá al terminar el cliclo");
      while (!Serial.available() ){

      //fwd test
        Serial.println("FWD, 0% > 100% > 0%");
        digitalWrite(MOT_L_A_PIN, HIGH);
        digitalWrite(MOT_L_B_PIN, LOW);
        digitalWrite(MOT_R_A_PIN, HIGH);
        digitalWrite(MOT_R_B_PIN, LOW);
        //0 > 100
        for(int i=0; i<255; i+=10){
          analogWrite(MOT_L_PWM_PIN, i);
          analogWrite(MOT_R_PWM_PIN, i);
          Serial.print(i);
          Serial.print("%\t ENC(L,R): ");
          Serial.print(l_pulses);
          Serial.print(' ');
          Serial.println(r_pulses);
          delay(250);
        }
        delay(1000);
        //100 >0
        for(int i=250; i>-1; i-=10){
          analogWrite(MOT_L_PWM_PIN, i);
          analogWrite(MOT_R_PWM_PIN, i);
          Serial.print(i);
          Serial.println('%');
          delay(250);
        }
        digitalWrite(MOT_L_PWM_PIN, LOW);
        digitalWrite(MOT_R_PWM_PIN, LOW);
        delay(2000);

      //rwd test
        Serial.println("RWD, 0% > 100% > 0%");
        digitalWrite(MOT_L_A_PIN, LOW);
        digitalWrite(MOT_L_B_PIN, HIGH);
        digitalWrite(MOT_R_A_PIN, LOW);
        digitalWrite(MOT_R_B_PIN, HIGH);
        //0 > 100
        for(int i=0; i<255; i+=10){
          analogWrite(MOT_L_PWM_PIN, i);
          analogWrite(MOT_R_PWM_PIN, i);
          Serial.print('-');
          Serial.print(i);
          Serial.println('%');
          delay(250);
        }
        delay(1000);
        //100>0
        for(int i=250; i>-1; i-=10){
          analogWrite(MOT_L_PWM_PIN, i);
          analogWrite(MOT_R_PWM_PIN, i);
          Serial.print('-');
          Serial.print(i);
          Serial.println('%');
          delay(250);
        }
        digitalWrite(MOT_L_PWM_PIN, LOW);
        digitalWrite(MOT_R_PWM_PIN, LOW);
        delay(2000);

        //BRAKE test
        Serial.println("BRAKE TEST, 0 > 100 BRAKE!");
        for(int i=0; i<255; i+=10){
          analogWrite(MOT_L_PWM_PIN, i);
          analogWrite(MOT_R_PWM_PIN, i);
          Serial.print(i);
          Serial.println('%');
          delay(100);
        }
        delay(2000);
        Serial.println("BRAKE!");
        digitalWrite(MOT_L_A_PIN, LOW);
        digitalWrite(MOT_L_B_PIN, LOW);
        digitalWrite(MOT_R_A_PIN, LOW);
        digitalWrite(MOT_R_B_PIN, LOW);
        
        delay(1000);
        digitalWrite(MOT_L_PWM_PIN, LOW);
        digitalWrite(MOT_R_PWM_PIN, LOW);
        
      }//END TEST
      break;
    case '6': //RGB LED
      Serial.println("TEST not implemented yet");
      while (!Serial.available() ){
        digitalWrite(L_RED_PIN, HIGH);
        digitalWrite(R_RED_PIN, HIGH);
        digitalWrite(L_GRE_PIN, LOW);
        digitalWrite(R_GRE_PIN, LOW);
        digitalWrite(L_BLU_PIN, LOW);
        digitalWrite(R_BLU_PIN, LOW);
        delay(1000);
  
        digitalWrite(L_RED_PIN, LOW);
        digitalWrite(R_RED_PIN, LOW);
        digitalWrite(L_GRE_PIN, HIGH);
        digitalWrite(R_GRE_PIN, HIGH);
        digitalWrite(L_BLU_PIN, LOW);
        digitalWrite(R_BLU_PIN, LOW);
        delay(1000);
  
        digitalWrite(L_RED_PIN, LOW);
        digitalWrite(R_RED_PIN, LOW);
        digitalWrite(L_GRE_PIN, LOW);
        digitalWrite(R_GRE_PIN, LOW);
        digitalWrite(L_BLU_PIN, HIGH);
        digitalWrite(R_BLU_PIN, HIGH);
        delay(1000);
  
        digitalWrite(L_RED_PIN, LOW);
        digitalWrite(R_RED_PIN, LOW);
        digitalWrite(L_GRE_PIN, LOW);
        digitalWrite(R_GRE_PIN, LOW);
        digitalWrite(L_BLU_PIN, LOW);
        digitalWrite(R_BLU_PIN, LOW);
      }
      break;
    case '7': //fall sensors
      Serial.println("Fall/Bumper Sensors");
      while (!Serial.available() ){
        Serial.print("Fall Sensors:\t");
        Serial.print("F: ");
        Serial.print(digitalRead(F_FALL_PIN) );
        Serial.print("\tL: ");
        Serial.print(digitalRead(L_FALL_PIN) );
        Serial.print("\tR: ");
        Serial.print(digitalRead(R_FALL_PIN) );
        Serial.print("\tB: ");
        Serial.println(digitalRead(B_FALL_PIN) );

        Serial.print("Bumper Sensors:\t");
        Serial.print("F: ");
        Serial.print(digitalRead(F_BUMP_PIN) );
        Serial.print("\tL: ");
        Serial.print(digitalRead(L_BUMP_PIN) );
        Serial.print("\tR: ");
        Serial.print(digitalRead(R_BUMP_PIN) );
        Serial.print("\tB: ");
        Serial.println(digitalRead(B_BUMP_PIN) );
        Serial.println(' ');
        delay(2000);
      }
      break;
    case '8': //Ultrasound
      while (!Serial.available() ){
        Serial.print("Distancia\tF: ");
        Serial.print(us_range(F_US_TRIG, F_US_ECHO));
        Serial.print("\tL: ");
        Serial.print(us_range(L_US_TRIG, L_US_ECHO));
        Serial.print("\tR: ");
        Serial.print(us_range(R_US_TRIG, R_US_ECHO));
        Serial.print("\tB: ");
        Serial.print(us_range(B_US_TRIG, B_US_ECHO));
        Serial.println("");
        //delay(500);
      }
      break;
    case '9': //UART
      Serial.print("Probando comunicación con la raspberry\nDurante 30 segundos, cualquier carácter recibido por esta terminal será enviado a la raspberry y viceversa\n");
      timeout = millis() + 30000;
      while(millis() < timeout){
        //Incming data from arduino
        if(Serial.available()){
          Serial2.write( Serial.peek());
          Serial3.write( Serial.read());
        }
        //Incoming data from BT
        if(Serial2.available())
          Serial.write(Serial2.read());
        //Incoming data from raspberry
        if(Serial3.available())
          Serial.write(Serial3.read());
      }
      break;
      
    default:
      Serial.println("ERROR: Comando no reconocido");
      break;
  }//END SWITCH
  
  print_message();
  delay(100);
  // If there is something in the buffer, clear it
  while (Serial.available() )
    Serial.read();
}//END lola_validate()

void print_message(){
  Serial.print("Selecciona el test a realizar [1-10]:\n");
  Serial.println("\t 1: Test de bateria");
  Serial.println("\t 2: Test de DIP Switches");
  Serial.println("\t 3: Test de Rear LED/SW");
  Serial.println("\t 4: Test de buzzer");
  Serial.println("\t 5: Test de Motores");
  Serial.println("\t 6: Test Led RGB");
  Serial.println("\t 7: Test Bumper/Fall sensor");
  Serial.println("\t 8: Ultrasound");
  Serial.println("\t 9: UART");
  Serial.print(": ");
}//END print_message()

int us_range(int TriggerPin, int EchoPin) {
   long duration, distanceCm;
   
   digitalWrite(TriggerPin, LOW);  //Keep the line LOW for 4ms, for a clean flank
   delayMicroseconds(4);
   digitalWrite(TriggerPin, HIGH);  //Generate a high transition
   delayMicroseconds(10);
   digitalWrite(TriggerPin, LOW);   //After 10us, lower the line
   
   duration = pulseIn(EchoPin, HIGH, 5080000);  //medimos el tiempo entre pulsos, en microsegundos
   
   distanceCm = duration * 10 / 292/ 2;   //convertimos a distancia, en cm
   if (distanceCm == 0)
    return -2;   
   if (distanceCm < 3000)
    return distanceCm;
  else
    return -1;
}
