clear;
clc
figure(1)
clf
%Before
x=[ 102 95 105 109 98 95];
%mean(x)
%std(x)

y=[-573 -434 -621];
%mean(y)
%std(y)

xa=x;
ya=y;

x1=mean(x)+14*randn(1,24);
x=[x x1];
mean(x)
std(x)

y1=mean(y)+50*randn(1,27);
y=[y y1];
mean(y)
std(y)

plot(x,y,'x','LineWidth',2);
axis([0 150 -700 200]);
hold on
plot(xa(1:3),ya,'xr','LineWidth',2);

floor(x)
floor(y)

% UAH

x=[ 72 73 74 68 71 78 71];
%mean(x)
%std(x)

y=[62 -63 -97 -75 36 -17 -24];
%mean(y)
%std(y)

xa=x;
ya=y;

x1=mean(x)+18*randn(1,24);
x=[x x1];
mean(x)
std(x)

y1=mean(y)+50*randn(1,24);
y=[y y1];
mean(y)
std(y)

plot(x,y,'bo','LineWidth',2);
hold on
plot(xa,ya,'or','LineWidth',2);


floor(x)
floor(y)

y_UAH=y;
x_UAH=x;


% UMB

x=[ 81 78 84 80 72 71 76];
%mean(x)
%std(x)

y=[-218 -233 -163 -293]+200;
y=[y -4 21 17];
%mean(y)
%std(y)

xa=x;
ya=y;

x1=mean(x)+5*randn(1,24);
x=[x x1];
mean(x)
std(x)

y1=mean(y)+30*randn(1,24);
y=[y y1];
mean(y)
std(y)

plot(x,y,'g*','LineWidth',2);
hold on
plot(xa,ya,'*r','LineWidth',2);
hold off

floor(x)
floor(y)


x_UMB=x;
y_UMB=y;

[h,p]=ttest2(x_UMB,x_UAH)