#!/usr/bin/env python
# -*- coding: utf-8 -*-


# Módulos
from PIL import Image, ImageOps
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import math
from os import path

from time import time
import sys


expanded_count = 0

gs = gridspec.GridSpec(1, 2)

aprox_array = []
xAprox, yAprox = 0,0


xInicial = 103
yInicial = 68

xFinal = 68
yFinal = 68


# Funciones
# ---------------------------------------------------------------------

def plotQR(mapa=None):

	QRNames = ['S34','S345','S344','S346','S343','S347','S342','S348','S341','SH','SM','PFP','VB1','VB2','ALM1','SE','AUX1','FCP','CEL1','CEL2']

	QR_X = [114,108,103,90,86,73,68,60,55,44,44,26,7,7,22,17,23,14,39,42]

	QR_Y = [68,72,66,72,64,71,64,72,65,72,65,64,67,71,91,92,42,36,41,22]
	
	
	for i in range(len(QRNames)):

		plt.plot(QR_X[i],  QR_Y[i], 'gD', lw=2, label="QR", picker=4)
		plt.annotate(QRNames[i], xy=(QR_X[i], QR_Y[i]), color="red", 
		   xytext=(QR_X[i]+3, QR_Y[i]+1),arrowprops=dict(arrowstyle="->", connectionstyle="arc3")
		   )




def onpick(event):
	global xFinal, yFinal, yRoom

	QRNames = ['S34','S345','S344','S346','S343','S347','S342','S348','S341','SH','SM','PFP','VB1','VB2','ALM1','SE','AUX1','FCP','CEL1','CEL2']


	QR_X = [114,108,103,90,86,73,68,60,55,44,44,26,7,7,22,17,23,14,39,42]

	QR_Y = [68,72,66,72,64,71,64,72,65,72,65,64,67,71,91,92,42,36,41,22]

	thisline = event.artist
	xdata = thisline.get_xdata()
	ydata = thisline.get_ydata()
	ind = event.ind
	points = tuple(zip(xdata[ind], ydata[ind]))
	print("Target x: ",  points[0][0])
	print("Target y: ",  points[0][1])
	xFinal = points[0][0]
	yFinal = points[0][1]
	yRoom = points[0][1]
	for i in range(len(QRNames)):
		if QR_Y[i] == points[0][1] and QR_X[i] == points[0][0]:
			plt.title("Destination fixed in %s"% QRNames[i])
	plt.draw()
	#Estaríamos en el pasillo si:
	if yFinal>63 and yFinal<73:
		yFinal = 68
   



def Aprox(aproxim_array=None):
	'''Funcion que dibuja en el mapa a los 6 segundos de haber comenzado el algoritmo el camino que se recorrería hasta el momento'''
	global xFinal, yFinal, xInicial, yInicial, gs, xAprox, yAprox, aprox_array
	
	if aproxim_array:

		Optimal_path = []
		inode = 0

		#Vamos buscando en OPEN (aproxim_array) para determinar el padre del nodo (target el primero)
		parent_x = aproxim_array[node_index(aproxim_array,xAprox,yAprox)][3]
		parent_y = aproxim_array[node_index(aproxim_array,xAprox,yAprox)][4]

		#Mientras no lleguemos a través de los padres al Nodo inicial:
		while parent_x!=xInicial or parent_y!=yInicial:
			
			Optimal_path.append([parent_x, parent_y])
			#Tomamos los "abuelos", (Los padres del nodo padre para continuar el loop)
			inode = node_index(aproxim_array, parent_x,parent_y)
			parent_x = aproxim_array[inode][3]
			parent_y = aproxim_array[inode][4]

		aprox_array = Optimal_path
		
	else:

		Optimal_path = aprox_array    
		
	Optimal_path_x = []
	Optimal_path_y = []
	
	for i in range(len(Optimal_path)):
		Optimal_path_x.append(Optimal_path[i][0])
		Optimal_path_y.append(Optimal_path[i][1])
	
	Optimal_path_x.append(xInicial)
	Optimal_path_y.append(yInicial)


	mapa1 = plt.subplot(gs[0]) 
	mapa1.plot(xInicial,  yInicial, 'bo', lw=2, label="Inicio")
	mapa1.annotate('Start', xy=(xInicial, yInicial), 
	   xytext=(xInicial+3, yInicial+1),arrowprops=dict(arrowstyle="->", connectionstyle="arc3")
	   )
	
	#mapa1.plot(xFinal,  yFinal, 'ro', lw=2, label="Final")
	#mapa1.annotate('Target', xy=(xFinal, yFinal), 
		#xytext=(xFinal+3, yFinal+1),arrowprops=dict(arrowstyle="->", connectionstyle="arc3")
		#)

	

	I = Image.open(path.dirname(path.realpath(sys.argv[0]))+'/Mapatrabajo-4.png')    
	mapa1.imshow(plt.imread(path.dirname(path.realpath(sys.argv[0]))+'/Mapatrabajo-4.png'))    
	mapa1.plot(Optimal_path_x, Optimal_path_y, 'm', label="Aprox")  

	mapa1.legend() 
	
	mapa1.axis('off')
	
	mapa1.text(2, -6, u" Camino encontrado en 6 segundos", fontsize=10)
	
	plotQR()
	plt.show()
   


def distance(x1, y1, x2, y2):
	'''Funcion que devuelve la distancia entre dos puntos del plano ( el mapa)'''
	return math.sqrt(abs(math.pow(x1-x2,2) + abs(math.pow(y1-y2,2))))

def insert_open(xval, yval, parent_xval, parent_yval, hn, gn, fn):
	'''Funcion que inserta en la lista abierta el nodo recibido con un formato específico'''
	return [1, xval, yval, parent_xval, parent_yval, hn, gn, fn]

#La primera vez, como hn recibimos el path_cost=0 del Nodo inicial
def expand_array(node_x, node_y, hn, xTarget,yTarget,CLOSED,MAX_X,MAX_Y):
	'''Funcion que devuelve la lista de los nodos vecinos que no estén en la lista cerrada'''
	exp_array = []
	global expanded_count
	global xInicial, yInicial, xFinal, yFinal

	#Dos bucles for para comprobar los 8 vecinos
	for k in [1 , 0, -1]:
		for j in [1, 0, -1]:
			#El nodo propio no puede ser sucesor, k=0:
			if k!=j or k!=0:
				s_x = node_x+k
				s_y = node_y+j

				#Si el Nodo candidato está dentro del array:
				if (s_x>0 and s_x <=MAX_X) and (s_y>0 and s_y<=MAX_Y):
					flag = 1

					#Comprobamos si el posible candidato está en la lista cerrada
					for i in range(len(CLOSED)):
						if s_x==CLOSED[i][0] and s_y==CLOSED[i][1]:
							flag = 0
					

					#Agregamos a la lista de sucesores los datos correspondientes
					if flag==1:
						#Coste de viajar hasta el nodo. La primera vez esta hn es 0 para el nodo inicial
						Gn = hn+distance(node_x, node_y, s_x, s_y)
						#Distancia entre el nodo y el objetivo
						Hn = distance(xTarget,yTarget,s_x,s_y)
						#Coste total Fn:
						Fn = Gn + Hn
						expanded_count +=1
##################      OJO QUE SI SE CAMBIA G POR H EN exp_array CAMBIA MUCHO EL ALGORITMO, PROBAR LA QUE MAS INTERESE    ###########################

#Si se interbambia a heurística Hn por el coste del propio camino Gn, obtenemos resultados con menos zig-zag pero se recorren mas metros
						exp_array.append([s_x, s_y, Gn, Hn, Fn])                

			
						
	#print(" %d nodos sucesores analizados " % expanded_count)
	return exp_array



def node_index(OPEN, xval, yval):
	'''Funcion que devuelve el índice del nodo recibido'''
	i=0
	
	while OPEN[i][1]!=xval or OPEN[i][2]!=yval:
		i+=1

	return i



def min_fn(OPEN, OPEN_COUNT, xTarget, yTarget):
	'''Devuelve el nodo con el minimo coste fn'''
	global s
	#array auxiliar para almacenar los de OPEN con el primer bit a 1, que significa que estan en la lista
	temp_array = []
	flag=0
	goal_index=0
	
	for j in range(OPEN_COUNT):
		
		if OPEN[j][0]==1:
			if len(OPEN[j])<=8:
				OPEN[j].append(j)
			temp_array.append(OPEN[j])

			
			#Si el sucesor es el Target:   
			if OPEN[j][1]==xTarget and OPEN[j][2]==yTarget:
				flag=1
				#Indice en OPEN del Tarjet
				goal_index=j
			

	#Si un sucesor es el nodo final, devolvemos como indice de menor coste F el nodo Target
	if flag==1:
		i_min = goal_index

	#Devolvemos el indice del nodo mas bajo
	if len(temp_array)!=0:
		fn_values =[]
		for i in range(len(temp_array)):
			fn_values.append(temp_array[i][7])
		
		temp_min = fn_values.index(min(fn_values))
		
		#Indice del nodo mas bajo referido a la lista OPEN
		i_min = temp_array[temp_min][8]
		
	else:
		#temp_array vacio. No hay mas caminos disponibles
		i_min = -1

	s=1
	return i_min

# ---------------------------------------------------------------------

def algoritmo():
	global xInicial, yInicial, xFinal, yFinal, gs, xAprox, yAprox, yRoom
	global expanded_count
	global tiempo_inicial
	#Dibujamos el mapa
	I = Image.open(path.dirname(path.realpath(sys.argv[0]))+'/MAPA2.png')

	#Convertimos la imagen I en una matriz uint8 para mejorar el rendimiento de la imagen
	mapa = np.asarray(I,dtype=np.float32)

	#Como la matriz resultante en mapa está invertida a la imagen, volteamos verticalmente la imagen
	I = ImageOps.flip(I)

	MAX_Y = len(mapa)
	MAX_X = len(mapa[0])
	print("Filas del array mapa = %d" % MAX_Y)
	print("Columnas del array mapa = %d" % MAX_X)

	#print(mapa.flags)
	#Cambio el flag del array para poder cambiar sus valores
	mapa.setflags(write=True)

	#Matriz que carga las coordenadas de cada objeto en el mapa
	MAP = 2*np.ones((MAX_Y, MAX_X))

	#Y transformamos nuestro array MAP a los valores de cada cosa
	for n in range(0, MAX_Y):
		for m in range(0, MAX_X):
			if mapa[n,m]<255:
				#Obstaculo
				MAP[n,m]=-1
			else:
				#Camino libre
				MAP[n,m]=2

	print("Mapa convertido")


	MAX_Y = len(MAP)
	MAX_X = len(MAP[0])
	print("MAX_X: %d" % MAX_X)
	print("MAX_Y: %d" % MAX_Y)


	fig = plt.figure()
	mapaselect = fig.add_subplot(111)
	
	
	mapaselect.plot(xInicial,  yInicial, 'bo', lw=2, label="Inicio")
	mapaselect.annotate('Start', xy=(xInicial, yInicial), 
	   xytext=(xInicial+3, yInicial+1),arrowprops=dict(arrowstyle="->", connectionstyle="arc3")
	   )
	
	#mapaselect.plot(xFinal,  yFinal, 'ro', lw=2, label="Final")
	#mapaselect.annotate('Target', xy=(xFinal, yFinal), 
	#    xytext=(xFinal+3, yFinal+1),arrowprops=dict(arrowstyle="->", connectionstyle="arc3")
	#    )

	plt.axis([0,MAX_X, MAX_Y, 0])
	mapaselect.legend()

		
	imagen = mapaselect.imshow(plt.imread(path.dirname(path.realpath(sys.argv[0]))+'/MAPA2.png'), cmap='gray')
	mapaselect.set_title("Choose your destination:")

	plotQR(mapaselect)

	fig.canvas.mpl_connect('pick_event', onpick)
	
	plt.show()

	xStart = xInicial
	yStart = yInicial

	xTarget = xFinal
	yTarget = yFinal

	MAP[yStart][xStart] = 1


	#Señalamos en el mapa el objetivo como 0
	MAP[yTarget][xTarget] = 0

	
   

	#Creamos las listas abierta y cerrada
	OPEN=[]
	CLOSED=[]

	#Guardamos en la lista CLOSED las coordenadas de los obstaculos 
	#las guardamos de la forma [x,y]
	for i in range(MAX_X):
		for j in range(MAX_Y):
			if MAP[j,i] == -1:
				CLOSED.append([i,j])
				

	#numero de items en la lista CLOSED menos uno ya que lo usaremos como indice
	CLOSED_COUNT = len(CLOSED)

	#Seleccionamos el nodo inicial como nodo de partida 
	xNode = xStart
	yNode = yStart

	#Y es el primer item de la lista abierta, la cual tambien NO contamos ya que
	#lo utilizaremos como indice
	OPEN_COUNT = 0

	path_cost = 0
	goal_distance = distance(xNode,yNode,xTarget,yTarget)

	print("La distancia total hasta el objetivo es %f " %goal_distance)
	

	
	

	#xval, yval, parent_xval, parent_yval, hn, gn, fn
	OPEN.append(insert_open(xNode,yNode,xNode,yNode,path_cost,goal_distance, goal_distance))
	
	OPEN[OPEN_COUNT][0]=0

	#Agregamos el nodo seleccionado, que es el inicial de momento , a la lista cerrada
	CLOSED.append([xStart, yStart])
	CLOSED_COUNT +=1

	NoPath = 1


	tiempo_inicial = time()
	
		#Empieza el algoritmo
	while (xNode!=xTarget or yNode!=yTarget) and NoPath==1:
		#exp_array -> array de los vecinos que cumplen las  condiciones
		exp_array=expand_array(xNode,yNode,path_cost,xTarget,yTarget,CLOSED,MAX_X,MAX_Y)
		
		#Numero de sucesores. Lo utilizaremos coom indice                        OJO Y NO RESTARLE UNO!!!!! funcion range()
		exp_count = len(exp_array)


		#FORMATO LISTA OPEN
		#-------------------------------------------------------------------------------------
		#EN LISTA? -> 1/0 | X val | Y val | Parent X val | Parent Y val | h(n) | g(n) | f(n) |
		#-------------------------------------------------------------------------------------
		#FORMATO exp_array
		#--------------------------------------
		#|X val | Y val |  h(n) | g(n) | f(n) |
		#--------------------------------------

		#Chequeamos para que si en la lista de sucesores
		#tenemos un Nodo que ya estaba en la lista OPEN, 
		#actualizamos el padre del nodo y los costes
		for i in range(exp_count):
			flag = 0

			for j in range(OPEN_COUNT):
				#Si el sucesor ya estaba en la lista abierta:
				if exp_array[i][0]==OPEN[j][1] and exp_array[i][1]==OPEN[j][2]:
					#si ahora como sucesor, tiene menor coste F, le asignamos el minimo
					OPEN[j][7] = min(OPEN[j][7], exp_array[i][4])

					#si es más pequeño el coste F(n) del sucesor de la lista que el de la lista abierta:
					if OPEN[j][7]==exp_array[i][4]:
						
						OPEN[j][3] = xNode
						OPEN[j][4] = yNode
						OPEN[j][5] = exp_array[i][2]
						OPEN[j][6] = exp_array[i][3]

						#tiempo = time()-tiempo_inicial
						
						#if  tiempo > 6 and tiempo < 7:
						#    xAprox = xNode
						#    yAprox = yNode
						#    Aprox(OPEN)

					flag = 1


			#Si no hay coincidencias de un sucesor en la lista OPEN, lo agregamos a ésta última
			if flag==0:
				OPEN_COUNT += 1
				OPEN.append(insert_open(exp_array[i][0], exp_array[i][1], xNode, yNode, exp_array[i][2], exp_array[i][3], exp_array[i][4]))
				

		#Buscamos el nodo con el valor FN mas bajo
		index_min_node = min_fn(OPEN, OPEN_COUNT, xTarget, yTarget)

		#Si no hay mas caminos disponibles:
		if index_min_node!=(-1):
			#Ponemos xNode e yNode como el nodo con el minimo Fn (de los sucesores)
			xNode = OPEN[index_min_node][1]
			yNode = OPEN[index_min_node][2]
			#Actualizamos el coste acumulado del viaje desde el sucesor con su G(n)
			path_cost = OPEN[index_min_node][5]
			#Movemos el nodo a la lista CLOSED
			CLOSED_COUNT+=1
			CLOSED.append([xNode, yNode])
			#Lo sacamos de la lista OPEN
			OPEN[index_min_node][0] = 0
		else:
			#no existe camino
			NoPath = 0

	
	#Una vez acabado el loop while en el que vamos analizando los sucesores y sus costes
	#El camino más óptimo es el que empieza desde el final y va hacia el Nodo inicial
	#siguiendo el camino de los padres que hemos ido comparando y anotando en el loop
	i = len(CLOSED)
	Optimal_path = []
	xval = CLOSED[-1][0]
	yval = CLOSED[-1][1]

	#Metemos en el camino optimo los valores del ultimo elemento metido a CLOSED
	Optimal_path.append([xval,yval])
	
	#Si el ultimo elemento fue el Target:
	if xval==xTarget and yval==yTarget:
		inode = 0
		#Vamos buscando en OPEN para determinar el padre del nodo (target el primero)
		parent_x = OPEN[node_index(OPEN,xval,yval)][3]
		parent_y = OPEN[node_index(OPEN,xval,yval)][4]

		#Mientras no lleguemos a través de los padres al Nodo inicial:
		while parent_x!=xStart or parent_y!=yStart:
			
			Optimal_path.append([parent_x, parent_y])
			#Tomamos los "abuelos", (Los padres del nodo padre para continuar el loop)
			inode = node_index(OPEN, parent_x,parent_y)
			parent_x = OPEN[inode][3]
			parent_y = OPEN[inode][4]
			
		
		Optimal_path_x = []
		Optimal_path_y = []
		
		for i in range(len(Optimal_path)):
			Optimal_path_x.append(Optimal_path[i][0])
			Optimal_path_y.append(Optimal_path[i][1])
		
		Optimal_path_x.append(xStart)
		Optimal_path_y.append(yStart)

		#mapa2 = plt.subplot(gs[1])

		plt.plot(xStart,  yStart, 'bo', lw=2, label="Inicio")
		plt.annotate('Start', xy=(xStart, yStart), 
			xytext=(xStart+3, yStart+1),arrowprops=dict(arrowstyle="->", connectionstyle="arc3")
			)

		plt.plot(xTarget,  yTarget, 'ro', lw=2, label="Final")
		plt.annotate('Target', xy=(xTarget, yTarget), 
			xytext=(xTarget+3, yTarget+1),arrowprops=dict(arrowstyle="->", connectionstyle="arc3")
			)

	
		plt.plot(Optimal_path_x, Optimal_path_y,'b-', lw=2)
		plt.imshow(plt.imread(path.dirname(path.realpath(sys.argv[0]))+'/MAPA2.png'), cmap='gray')
		tiempo_final = time()
		print("Tiempo de ejecución:")
		tiempo_ejecucion = tiempo_final-tiempo_inicial
		print("%0.10f segundos"%tiempo_ejecucion)

		plt.text(2, -6, u"Camino encontrado en {0:.2f} segundos".format(tiempo_ejecucion), fontsize=10)
		plt.legend()
		plt.axis('off')

		#Aprox()
		#plotQR()

		#plt.show()
		


	else:
		print("Lo siento, no existe camino hasta el objetivo")
		plt.imshow(plt.imread(path.dirname(path.realpath(sys.argv[0]))+'/MAPA2.png'), cmap= 'gray')
		plt.legend()
		plt.show()

	return (Optimal_path,yRoom)
	sys.exit(0)

if __name__ == '__main__':
	algoritmo()
