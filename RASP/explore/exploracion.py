#!/usr/bin/env python
# -*- coding: utf-8 -*-

#Basic library
import matplotlib.pyplot as plt
import numpy as np
import serial
import time
import math
import os
import pygame
from polly_tts import *
import sys
from os import path
import CalculoPose
import Aestrella
import funcionGirar2
import cv2
#Avance lineal por pulso del encoder en mm
cm = 0.2110

#Q=funcionQR.QR()
#Inicializaciones
escala=3 #pixeles/cm
marca=40 #cm/marca
plano=CalculoPose.PlanoResultados((600, 400), escala*marca)
d=2.5
puntos=np.matrix([
    [-d, -d, 0],
    [-d, +d, 0],
    [+d, +d, 0],
    [+d, -d, 0]], np.float32)

#Start posicion robot 
ANG=0
X=0
Y=0
enqIzq=0
enqDer=0

ser = 0
MAPSCALE = 200
anguloCamino = 0
i = 0
Optimal_path = 0

QRNames = ['S34','S345','S344','S346','S343','S347','S342','S348','S341','SH','SM','PFP','VB1','VB2','ALM1','SE','AUX1','FCP','CEL1','CEL2']

QR_X = [114,108,103,90,86,73,68,60,55,44,44,26,7,7,22,17,23,14,39,42]
QR_Y = [68,72,66,72,64,71,64,72,65,72,65,64,67,71,91,92,42,36,41,22]

def distance(x1, y1, x2, y2):
	'''Funcion que devuelve la distancia entre dos puntos del plano ( el mapa)'''
	return math.sqrt(abs(math.pow(x1-x2,2) + abs(math.pow(y1-y2,2))))


#Funcion avance
def avanzar(ser,ang0,x0,y0,distancia,Xdestino,Ydestino,nodosRecorrer,movimiento):

	avance = str(distancia)
	#variables inicio posicion
	theta = math.radians(ang0)    
	#print("Angulo inicial en radianes = {}".format(ang))
	#variables calculo movimiento

	#Avance lineal por pulso del encoder en mm

	#Nuestro diferencial de movimiento lo asociamos a delta numero de pulsos del encoder
	Delta = 15
	#Longitud del eje entre ruedas (mm)
	L = 190
	x = x0
	y = y0
	ValorD_delta = 0
	ValorI_delta = 0
	ValorD=0
	ValorI=0
	sR,sC,sL = 0,0,0
	theta_delta = 0
	derAnt=0
	izqAnt=0
	pulsosDER, pulsosIZQ = 0,0
	distanciaDER = 0
	auxDER,auxIZQ=0,0
	#variable para verificar que a parado
	iguales=0

	valor= []

	#ser.write('<'.encode('ascii'))
	#valor=ser.readline().decode('ascii')
	#print("Reset encoders")
	#Hasta que no tengamos una lectura clara, leyemos el puerto serie solicitando lectura de encoders 
	#lectura de encoders es el char ';'
	#while (valor[0:4]!=')0*0'):
	#	ser.write(';'.encode('ascii'))
	#	valor=ser.readline().decode('ascii')
		
	#print(valor)
	#Comenzamos mandando la orden y leyendo los encoder
	ser.write(('4'+ avance).encode('ascii'))

	ser.write(';'.encode('ascii'))
	valor=ser.readline().decode('ascii')
	#print("Leyendo encoders por prmera vez")
	#print(valor)

	#Comprobamos que durante 10 veces los encoder permanecen quietos para asegurar que no hay movimiento
	while (iguales<10):
		ser.write(';'.encode('ascii'))
		valor=ser.readline().decode('ascii')
		#print("valor en mitad de giro = {}".format(valor))
		if valor[0]!=')':       #condicion para evitar leer basura
			continue
		#Deacuerdo al formato recibido, los valores se separan por el *
		ValorII,ValorDD=valor[1:-2].split('*')
		#ValorI_anterior y ValorD_anterior son los pulsos de los encoder. Fijamos la distancia delta
		ValorI=int(ValorII)
		#print("ValorI = {}".format(ValorI))
		ValorD=int(ValorDD)
		
		
		if ValorD>ValorD_delta or ValorI>ValorI_delta:
			auxDER = ValorD - pulsosDER
			auxIZQ = ValorI - pulsosIZQ

			pulsosDER = ValorD
			pulsosIZQ = ValorI
		
			sL = cm*auxIZQ
			sR = cm*auxDER
			#valor absoluto para no diferenciar si la desviación fue a izquierda o derecha
			theta_delta = (sR-sL)/L
			sC = (sR+sL)/2
			
			#Actualizamos las variables de la nueva posicion
			theta = theta + theta_delta
			x = x + sC*math.cos(theta)
			y = y - sC*math.sin(theta)

			ValorD_delta = ValorD + Delta
			ValorI_delta = ValorI + Delta


		if derAnt==ValorD:
			#print("IGUALES")
			iguales+=1
		else:
			iguales=0
			derAnt=ValorD
			izqAnt=ValorI
			
		comprobarANG(Xdestino,Ydestino,nodosRecorrer, x, y, movimiento)


	#print("Funcion girar izquierda finalizada")
	ser.write('?'.encode('ascii'))
	time.sleep(0.5)

	return (x,y,math.degrees(theta),ValorI,ValorD)

	
def comprobarANG(Xdestino=None,Ydestino=None, nodosRecorrer=None, x_camino=None, y_camino=None, movimiento=None):
	global X,Y,ANG, MAPSCALE, anguloCamino, ser, i, Optimal_path
	if movimiento=='horizontal':
		y_camino = y_camino/MAPSCALE
		diferencia = abs(Ydestino-y_camino)
		
		if diferencia > 1.5:
			#Lo primero paramos los motores
			ser.write('?'.encode('ascii'))
			#print("X = {}  Y = {}".format(X,Y))
			#print("Xdestino = {} e Ydestino = {}".format(Xdestino , Ydestino))
			hipotenusa = distance(X,Y,Xdestino,Ydestino)
			#print("Hipotenusa: {}".format(hipotenusa))
			catetoContiguo = abs(Xdestino-X)
			#print("Cateto contiguo: {}".format(catetoContiguo))
			gradosCorregir = math.degrees(math.acos(catetoContiguo/hipotenusa))
			#print("Grados a corregir: {}".format(int(gradosCorregir)))
			#Definimos la distancia a recorrer tras corregir
			n = int(hipotenusa)

			#Si vamos hacia la izquierda y nuestra coordenada Y es más baja , corregimos a izquierda
			if Xdestino-X < 0 and Ydestino-Y > 0:
				print("Se corregira a izquierda, orientacion a izquierda del mapa")
				X,Y,ANG,enqIzq,enqDer = funcionGirar2.izq(ser,ANG,int(gradosCorregir),X*MAPSCALE, Y*MAPSCALE)
				print("Grados corregidos a izquierda: {}".format(int(gradosCorregir)))
				X /= MAPSCALE
				Y /= MAPSCALE
				if n<10:
					X,Y,ANG,enqIzq,enqDer = avanzar(ser,ANG,X*MAPSCALE,Y*MAPSCALE,(n/10)*MAPSCALE,Xdestino,Ydestino,nodosRecorrer,'horizontal')
				X,Y,ANG,enqIzq,enqDer = avanzar(ser,ANG,X*MAPSCALE,Y*MAPSCALE,n*MAPSCALE,Xdestino,Ydestino,nodosRecorrer,'horizontal')
				i += n
				print ("Nodos avanzados: {}".format(i))
				X /= MAPSCALE
				Y /= MAPSCALE
				print("Las nuevas coordenadas en Nodos tras avanzar son \n X = {} Y = {} ANG= {}".format(X,Y,ANG))
				print("Que en milimetros son : X = {}, Y = {} ANG = {}".format(X*MAPSCALE, Y*MAPSCALE, ANG))


			#Si vamos hacia la izquierda y nuestra coordenada Y es más alta , corregimos a derecha
			elif (Xdestino-X < 0) and (Ydestino-Y < 0):
				print("Se corregira a derecha, orientacion a izquierda del mapa")
				X,Y,ANG,enqIzq,enqDer = funcionGirar2.der(ser,ANG,int(gradosCorregir),X*MAPSCALE, Y*MAPSCALE)
				X /= MAPSCALE
				Y /= MAPSCALE
				print("Grados a corregidos a derecha: {}".format(int(gradosCorregir)))
				if n<10:
					X,Y,ANG,enqIzq,enqDer = avanzar(ser,ANG,X*MAPSCALE,Y*MAPSCALE,(n/10)*MAPSCALE,Xdestino,Ydestino,nodosRecorrer,'horizontal')
				X,Y,ANG,enqIzq,enqDer = avanzar(ser,ANG,X*MAPSCALE,Y*MAPSCALE,n*MAPSCALE,Xdestino,Ydestino,nodosRecorrer,'horizontal')
				i += n
				print ("Nodos avanzados: {}".format(i))
				X /= MAPSCALE
				Y /= MAPSCALE
				print("Las nuevas coordenadas en Nodos tras avanzar son \n X = {} Y = {} ANG= {}".format(X,Y,ANG))
				print("Que en milimetros son : X = {}, Y = {} ANG = {}".format(X*MAPSCALE, Y*MAPSCALE, ANG))

			#Si vamos hacia la derecha y nuestra coordenada Y es más alta , corregimos a izquierda
			elif Xdestino-X > 0 and Ydestino-Y < 0:
				print("Se corregira a izquierda, orientacion a derecha del mapa")
				X,Y,ANG,enqIzq,enqDer = funcionGirar2.izq(ser,ANG,int(gradosCorregir),X*MAPSCALE, Y*MAPSCALE)
				print("Grados a corregidos a izquierda: {}".format(int(gradosCorregir)))
				X /= MAPSCALE
				Y /= MAPSCALE
				if n<10:
					X,Y,ANG,enqIzq,enqDer = avanzar(ser,ANG,X*MAPSCALE,Y*MAPSCALE,(n/10)*MAPSCALE,Xdestino,Ydestino,nodosRecorrer,'horizontal')
				X,Y,ANG,enqIzq,enqDer = avanzar(ser,ANG,X*MAPSCALE,Y*MAPSCALE,n*MAPSCALE,Xdestino,Ydestino,nodosRecorrer,'horizontal')
				i += n
				print ("Nodos avanzados: {}".format(i))
				X /= MAPSCALE
				Y /= MAPSCALE
				print("Las nuevas coordenadas en Nodos tras avanzar son \n X = {} Y = {} ANG= {}".format(X,Y,ANG))
				print("Que en milimetros son : X = {}, Y = {} ANG = {}".format(X*MAPSCALE, Y*MAPSCALE, ANG))
			

			#Si vamos hacia la derecha y nuestra coordenada Y es más baja , corregimos a derecha
			elif Xdestino-X > 0 and Ydestino-Y > 0:
				print("Se corregira a derecha, orientacion a derecha del mapa")
				X,Y,ANG,enqIzq,enqDer = funcionGirar2.der(ser,ANG,int(gradosCorregir),X*MAPSCALE, Y*MAPSCALE)
				print("Grados a corregidos a derecha: {}".format(int(gradosCorregir)))
				X /= MAPSCALE
				Y /= MAPSCALE
				if n<10:
					X,Y,ANG,enqIzq,enqDer = avanzar(ser,ANG,X*MAPSCALE,Y*MAPSCALE,(n/10)*MAPSCALE,Xdestino,Ydestino,nodosRecorrer,'horizontal')
				X,Y,ANG,enqIzq,enqDer = avanzar(ser,ANG,X*MAPSCALE,Y*MAPSCALE,n*MAPSCALE,Xdestino,Ydestino,nodosRecorrer,'horizontal')
				i += n
				print ("Nodos avanzados: {}".format(i))
				X /= MAPSCALE
				Y /= MAPSCALE
				print("Las nuevas coordenadas en Nodos tras avanzar son \n X = {} Y = {} ANG= {}".format(X,Y,ANG))
				print("Que en milimetros son : X = {}, Y = {} ANG = {}".format(X*MAPSCALE, Y*MAPSCALE, ANG))
		

	#input("presiona Intro para continuar\nAngulo es correcto")
	
def avanzarRecto(movimiento=None,nodosRecorrer=None,Xdestino=None, Ydestino=None):
	global X,Y,ANG, MAPSCALE, anguloCamino, i, ser, Optimal_path, yRoom
	#Se sobreentiende siempre una posición relativa al mapa de 90º (mirando al norte del mapa)
	print("Pasa 3")
	
	if movimiento=='inicial':
		
		iteracion=0
		#Comprobacion del primer giro Si la coordenada X es menor , es a izquierda
		if Optimal_path[0][0]-Optimal_path[1][0] == 1:
			print("Girar inicialmente 90 grados hacia la izquierda")
			X,Y,ANG,enqIzq,enqDer = funcionGirar2.izq(ser,ANG,90,X*MAPSCALE, Y*MAPSCALE)
			X /= MAPSCALE
			Y /= MAPSCALE
			#Si giramos a izquierda, disminuimos medio nodo en la distancia global -0.5
			#Sabiendo si el ultimo giro es a derecha o izuierda, compensamos o no ese nodo
			#Si el ultimo giro es a izquierda, habría que recorrer -0.5 - 0.5 nodos. 
			if yRoom>68 and yRoom<78:
				n= abs(Optimal_path[0][0]-Xdestino) #Nos quedamos un nodo cortos
			#Si el giro es a derecha en el pasillo, habría que recorrer medio nodo mas (+0.5). Por lo que hay que recorrer los nodos de distancia tal cual
			elif  yRoom<68 and yRoom>60:
				n= abs(Optimal_path[0][0]-Xdestino)+1 #A la diferencia de nodos hay que sumarle el nodo final	
				
			print("Las nuevas coordenadas en Nodos tras girar a izquierda son \n X = {} Y = {} ANG= {}".format(X,Y,ANG))
			print("En milimetros : X= {} Y = {} ANG = {}".format(X*MAPSCALE,Y*MAPSCALE, ANG))
			#input("presiona Intro...")
			giroinicial = 'izq'

		#Si por el contrario la coordenada X es mayor, es hacia la derecha
		elif Optimal_path[0][0]-Optimal_path[1][0] == -1:
			print("Girar inicialmente 90 grados hacia la derecha")
			X,Y,ANG,enqIzq,enqDer = funcionGirar2.der(ser,ANG,90,X*MAPSCALE, Y*MAPSCALE)
			X /= MAPSCALE
			Y /= MAPSCALE
			#Si el primer giro es a derecha , habría que aumentar +0.5 nodos el recorrido general
			#si el ultimo giro sera otra vez a la derecha, habría que recorrer un nodo más de la distancia 
			if yRoom>68 and yRoom<78:
				n= abs(Optimal_path[0][0]-Xdestino)+2
			#Si el giro es a izquierda en el pasillo, habría que recorrer medio nodo menos (-0.5). Por lo que nos quedamos igual
			elif  yRoom<68 and yRoom>60:
				n= abs(Optimal_path[0][0]-Xdestino)+1 #A la diferencia de nodos hay que sumarle el nodo final	
			print("Las nuevas coordenadas tras girar derecha son \n X = {} Y = {} ANG= {}".format(X,Y,ANG))
			giroinicial = 'der'
		
	while nodosRecorrer>i:
		print ("Nodos a recorrer: {}".format(n))
		if n<10:
			X,Y,ANG,enqIzq,enqDer = avanzar(ser,ANG,X*MAPSCALE,Y*MAPSCALE,(n/10)*MAPSCALE,Xdestino,Ydestino,nodosRecorrer,'horizontal')	
		else:
			X,Y,ANG,enqIzq,enqDer = avanzar(ser,ANG,X*MAPSCALE,Y*MAPSCALE,n*MAPSCALE,Xdestino,Ydestino,nodosRecorrer,'horizontal')
		i += int(n)
		print ("Nodos avanzados: {}".format(i))	
		X /= MAPSCALE
		Y /= MAPSCALE
		print("Las nuevas coordenadas en Nodos tras avanzar son \n X = {} Y = {} ANG= {}".format(X,Y,ANG))
		print("Que en milimetros son : X = {}, Y = {} ANG = {}".format(X*MAPSCALE, Y*MAPSCALE, ANG))
		#input("Presiona Intro...")
		
		#comprobarANG(Xdestino, Ydestino, nodosRecorrer)
		#iteracion+=1
		#print("Iteracion = {}\n".format(iteracion))

	if yRoom>68 and yRoom<78:
		if giroinicial=='izq':
			X,Y,ANG,enqIzq,enqDer = funcionGirar2.izq(ser,ANG,90,X*MAPSCALE, Y*MAPSCALE)
		else:
			X,Y,ANG,enqIzq,enqDer = funcionGirar2.der(ser,ANG,90,X*MAPSCALE, Y*MAPSCALE)
		X /= MAPSCALE
		Y /= MAPSCALE
	elif  yRoom<68 and yRoom>60:
		if giroinicial=='izq':
			X,Y,ANG,enqIzq,enqDer = funcionGirar2.der(ser,ANG,90,X*MAPSCALE, Y*MAPSCALE)
		else:
			X,Y,ANG,enqIzq,enqDer = funcionGirar2.izq(ser,ANG,90,X*MAPSCALE, Y*MAPSCALE)
		X /= MAPSCALE
		Y /= MAPSCALE
	
			  

def comprobarInclinacion(i):
	global Optimal_path
	#Si la coordenada x siguiente es mayor, vamos a la derecha y comprobamos coordenada y
	if Optimal_path[i+1][0] > Optimal_path[i][0]:
		#Si la y es mayor. SURESTE
		if Optimal_path[i+1][1] > Optimal_path[i][1]:	
			return 'SE'
		#Si por el contrario la y es menor. NORESTE
		elif Optimal_path[i+1][1] < Optimal_path[i][1]:
			return 'NE'		

	#Si la coordenada x siguiente es menor, vamos a la izquierda y comprobamos coordenada y
	if Optimal_path[i+1][0] < Optimal_path[i][0]:
		#Si la y es mayor. SUROESTE
		if Optimal_path[i+1][1] > Optimal_path[i][1]:
			return 'SW'
		#Si por el contrario la y es menor. NOROESTE
		if Optimal_path[i+1][1] < Optimal_path[i][1]:
			return 'NW'
	
	#Si la coordenada x es igual, analizamos norte o sur
	if Optimal_path[i+1][0] == Optimal_path[i][0]:
		#Si la y es mayor. SUR
		if Optimal_path[i+1][1] > Optimal_path[i][1]:	
			return 'SO'
		#Si por el contrario la y es menor. NORTE
		elif Optimal_path[i+1][1] < Optimal_path[i][1]:
			return 'NO'
	
	#Si la coordenada y es la misma, analizamos este u oeste
	if Optimal_path[i+1][1] == Optimal_path[i][1]:	
		#Si la x es mayor. ESTE
		if Optimal_path[i+1][0] > Optimal_path[i][0]:	
			return 'EA'
		#Si por el contrario la x es menor . OESTE
		elif Optimal_path[i+1][0] < Optimal_path[i][0]:
			return 'WE'	


def comprobarQuiebros():
	quiebros = []
	direccion = []
	#Primera inclinacion del camino
	print("Pasa 4")
	orientacion = comprobarInclinacion(0)
	direccion.append(orientacion)
	print("Pasa 5")
	#Recorremos el camino devuelto por el algoritmo Aestrella para analizar donde están los quiebros
	for index in range(len(Optimal_path)-1):
		#Analizamos la orientacion del siguiente nodo
		orientacion = comprobarInclinacion(index)
		#Si es la misma orientacion que la que teniamos en el array direccion, continuamos con el siguiente nodo
		if direccion[-1]==orientacion:
			continue
		#En cambio, si cambiamos la orientacion, apuntamos la nueva direccion así como el índice de Optimal_path donde se quiebra
		else:
			direccion.append(orientacion)
			quiebros.append(index)
			
	#Cuando termine el bucle devolvemos donde se hacen los quiebros y en qué direcciones
	return quiebros, direccion
		

#######################################################################
#######################################################################
def mover_recto(distancia):
	#Comando de avance recto con la distancia en mm
	if (distancia>9999):
		distancia=9999
	distancia=round(distancia)
	avance=str(distancia).zfill(4)
	print("Avance  ",avance)
	ser.write(('4'+ avance).encode('ascii'))

	#Comando para la lectura de los encoders
	#Cuando los encoders dejan de modificar ha llegado a su destino
	ser.write(';'.encode('ascii'))
	time.sleep(1)
	valor1=ser.readline().decode('ascii')
	#print("Leyendo encoders por prmera vez")
	#print(valor1)
	valor0=[]
	#Leemos hasta que los encoders se repiten
	#debería de incluir un time-out por si ocurre un error.
	while(valor1!=valor0):
		valor1=valor0
		ser.write(';'.encode('ascii'))
		time.sleep(0.5)
		valor0=ser.readline().decode('ascii')
		#print(valor0)
		# Si la distancia recorrida es mayor de la indicada
		# pasamos comando de parar, esto no debería ser necesario
		# pero es una medida de seguridad (Se ha dejado un margen
		# de 400 pulsos.
		#####################
		# OJO:     REVISAR ESTA COMPROBACION PARA QUE NO DE ERROR
		valoraux=valor0[1:3]
		#print(valoraux)
		if (float(valoraux)>distancia*cm+400):
			ser.write('?'.encode('ascii'))
			print("Parada para evitar error")

##############################################################
#
#  Esta función analiza las celdas indicadas en OP para navegar.
#  Descompone el recorrido en tramos rectos y realiza el giro
#  ante cada tramo si es necesario, recorre el tramo y finalmente
#  si es necesario gira para dejar el robot en la orientacion
#  indicada por orientacion_final.
#
#
##############################################################
def ejecutar_mov(OP,X,Y,ANG,Orientacion_final):
	UM=0  #Índice de la última posicion del array OP que se ha recorrido

	# Avazamos por todo el recorrido OP en tramos rectos 
	# con el correspondiente giro entre cada dos tramos rectos
	while(UM<len(OP)-1):
		print("Tramos",UM,"/",len(OP))
		# Tomamos las referencias de la primera direccion
		# que tomará el robot con la siguiente celda
		Rx=OP[UM+1][0]-OP[UM][0]
		Ry=OP[UM+1][1]-OP[UM][1]
		# Comprobamos cuanto tiempo permanece la dirección
		# inicial para hacer todo el tramo recto
		for mm2 in range(UM+2,len(OP)):
			if (Rx!=(OP[mm2][0]-OP[mm2-1][0]) or 
                            Ry!=(OP[mm2][1]-OP[mm2-1][1])):
        			mm2=mm2-1
        			break
		# Se realiza el giro necesario para abordar el tramo recto
		Angulo_movimiento=math.atan2(-Ry,Rx)
		Angulo_movimiento=Angulo_movimiento*360/6.28319
		if (ANG!=Angulo_movimiento):
			girar=Angulo_movimiento-ANG
			girar=round(girar)
			print("GIRAR  ",girar)
			if (girar>0):
		                X,Y,ANG,enqIzq,enqDer = funcionGirar2.izq(ser,ANG,int(girar),X*MAPSCALE,Y*MAPSCALE)
			else:
                                X,Y,ANG,enqIzq,enqDer = funcionGirar2.der(ser,ANG,int(-girar),X*MAPSCALE,Y*MAPSCALE)
			ANG=Angulo_movimiento
			#print(ANG)
		# Calculamos distancia a recorrer para recorrer el tramo recto
		dd=(OP[UM][0]-OP[mm2][0])**2+(OP[UM][1]-OP[mm2][1])**2
		dd=math.sqrt(dd)
		#print("Mover")
		#print(dd*MAPSCALE)
		distancia=round(dd*MAPSCALE)
		mover_recto(dd*MAPSCALE)
		UM=mm2;
	#print("Orientacion_final")
	#print(Orientacion_final)
	#print(ANG)

	if (ANG!=Orientacion_final):
		girar=Orientacion_final-ANG
		girar=round(girar)
		print("GIRAR  ",girar)
		if (girar>0):
			X,Y,ANG,enqIzq,enqDer = funcionGirar2.izq(ser,ANG,girar,X*MAPSCALE,Y*MAPSCALE)
		else:
			X,Y,ANG,enqIzq,enqDer = funcionGirar2.der(ser,ANG,-girar,X*MAPSCALE,Y*MAPSCALE)

		ANG=Orientacion_final
##############################################################
#
#  Esta función gira y recorre los tramos rectos indicados
#  por los vertices POINTS, finalmente
#  si es necesario gira para dejar el robot en la orientacion
#  indicada por orientacion_final.
#
#
##############################################################
def ejecutar_mov_vertices(POINTS,X,Y,ANG,Orientacion_final):
	UM=0  #Índice de la última posicion del array POINTS que se ha recorrido

	# Avazamos por todo el recorrido POINTS en tramos rectos 
	# con el correspondiente giro entre cada dos tramos rectos
	while(UM<len(POINTS)-1):
		print("Tramos",UM,"/",len(POINTS), "Desde",POINTS[UM], "a", POINTS[UM+1])
		# Tomamos las referencias de la primera direccion
		# que tomará el robot con la siguiente celda
		Rx=POINTS[UM+1][0]-POINTS[UM][0]
		Ry=POINTS[UM+1][1]-POINTS[UM][1]
		print(" Rx Ry  ",Rx,Ry)
		# Se realiza el giro necesario para abordar el tramo recto
		Angulo_movimiento=math.atan2(Ry,Rx)
		Angulo_movimiento=Angulo_movimiento*360/6.28319
		print("Ang. mov  ",Angulo_movimiento)
		if (ANG!=Angulo_movimiento):
			girar=Angulo_movimiento+ANG
			girar=round(girar)
			#print("Ang. mov", Angulo_movimiento,"GIRAR  ",girar)
			print("GIRAR  ",girar)
			if (girar>0):
				if (girar>180):
					X,Y,ANG,enqIzq,enqDer = funcionGirar2.der(ser,ANG,int(360-girar),X*MAPSCALE,Y*MAPSCALE)
				else:
					X,Y,ANG,enqIzq,enqDer = funcionGirar2.izq(ser,ANG,int(girar),X*MAPSCALE,Y*MAPSCALE)
			else:
				print("44444")
				#X,Y,ANG,enqIzq,enqDer = funcionGirar2.izq(ser,ANG,90,X*MAPSCALE,Y*MAPSCALE)
   
				if (girar<(-180)):
					print("55555")
					var_int=int(360+girar)
					
					print(var_int)
					X,Y,ANG,enqIzq,enqDer = funcionGirar2.izq(ser,ANG,var_int,X*MAPSCALE,Y*MAPSCALE)
				else:
					X,Y,ANG,enqIzq,enqDer = funcionGirar2.der(ser,ANG,int(-girar),X*MAPSCALE,Y*MAPSCALE)
  



			#if (girar>0):
			#	 X,Y,ANG,enqIzq,enqDer = funcionGirar2.izq(ser,ANG,girar,X*MAPSCALE,Y*MAPSCALE)
			#else:
			#	X,Y,ANG,enqIzq,enqDer = funcionGirar2.der(ser,ANG,-girar,X*MAPSCALE,Y*MAPSCALE)
			ANG=Angulo_movimiento
			#print("ANG",ANG)
		# Calculamos distancia a recorrer para recorrer el tramo recto
		dd=(POINTS[UM][0]-POINTS[UM+1][0])**2+(POINTS[UM][1]-POINTS[UM+1][1])**2
		dd=math.sqrt(dd)
		#print("Mover")
		#print(dd*MAPSCALE
		distancia=round(dd*MAPSCALE)
		mover_recto(dd*MAPSCALE)
		UM=UM+1;

	# Tras finalizar se orienta con la indicación de orientacion_final
	if (ANG!=Orientacion_final):
		girar=Orientacion_final-ANG
		girar=round(girar)
		print("GIRAR  PP:  ",girar)
		if (girar>0):
			if (girar>180):
				X,Y,ANG,enqIzq,enqDer = funcionGirar2.der(ser,ANG,int(360-girar),X*MAPSCALE,Y*MAPSCALE)
			else:
				X,Y,ANG,enqIzq,enqDer = funcionGirar2.izq(ser,ANG,girar,X*MAPSCALE,Y*MAPSCALE)
		else:
			print("Pasa por aqui")
			if (girar<(-180)):
				var_int=360+girar
				print("Var intermedia",var_int)

				X,Y,ANG,enqIzq,enqDer = funcionGirar2.izq(ser,ANG,int(var_int),X*MAPSCALE,Y*MAPSCALE)
			else:
				X,Y,ANG,enqIzq,enqDer = funcionGirar2.der(ser,ANG,int(-girar),X*MAPSCALE,Y*MAPSCALE)
  
		ANG=Orientacion_final

##############################################################
##############################################################
def main():
	cam=cv2.VideoCapture(0)
	#Resolución Width
	cam.set(3,1024)
	#Resolución Height
	cam.set(4,768)

	res, img=cam.read()
	tam=img.shape[0:2]
	f=0.79*tam[1]
	K=np.matrix([
	[f, 0, tam[1]/2],
	[0, f, tam[0]/2],
	[0, 0,        1]], np.float32)

	#Por el momento solo está implementado el camino horizontal en el mapa. La referencia de desviación por tanto es la coordenada Y
	global X,Y,ANG, MAPSCALE, anguloCamino, i, ser, Optimal_path, yRoom 
	#La variable global i es la cantidad de nodos recorridos
	ANG = 90
	anguloCamino = ANG
	nodosRecorrer = 0
	#indice variable auxiliar para ir marcando los quiebros
	indice = 0
	quiebros = []
	direcciones = []

	pygame.mixer.init()

	try:
		#print("Archivo ejecutado desde :")
		#print(path.dirname(path.realpath(sys.argv[0])))
		rutaAudios = "mpg123 -q "+path.dirname(path.realpath(sys.argv[0]))+"/audios"
		#speak = polly.VoiceSynthesizer(1.0, TextType='ssml', tts_online=True)
		prueba = 0
		#Inicializacion previa conexion serial
		ser=serial.Serial('/dev/ttyAMA0',9600,timeout=3)
		print ("Conectado")
		time.sleep(4);

		ser.write('<'.encode('ascii'))
		#Fin inicializacion
		#os.system(rutaAudios+"/estoyEnPosicion.mp3 &")
		#speak.say("<speak>Estoy en la posición señalizada en el mapa. Por favor, elija su destino</speak>")
		#while speak.IsSpeaking():
		#	pass		

		#Optimal_path, yRoom = Aestrella.algoritmo()
		#print("Salida Aestrella")
		#print(Optimal_path)
		#print( yRoom)
		yRoom=68
		Optimal_path=[]
		for ccc in range(6):
                                        Optimal_path.append([])
                                        Optimal_path[ccc]=[86+ccc,68]
		#Optimal_path.append([])
		#Optimal_path[-1]=[91,67]
		#Optimal_path.append([])
		#Optimal_path[-1]=[91,66]
		for ccc in range(7):
			Optimal_path.append([])
			Optimal_path[-1]=[92-ccc,68]
		#print(Optimal_path)
		#Ya que el camino recibido va desde el target al inicio, le damos la vuelta para que el indice 0 sea el inicio
		#Optimal_path = Optimal_path[::-1]
		#print("El camino sera :") print(Optimal_path)
		#Coordenadas iniciales
		X = Optimal_path[0][0]
		Y = Optimal_path[0][1]



		Orientacion_final=90
		nnn=0
		while(nnn<1):
			print("===============")
			print("===============")
			nnn=nnn+1
			#Leemos posicion inicial
			res, img=cam.read()
			if not res: break
			try:
				r, p=CalculoPose.CalculoPoseQR(img, puntos, K)
				plAux=np.copy(plano)
				R, _=cv2.Rodrigues(r)
				R=np.matrix(R)
				R2=R.T
				p2=R2*p
				print(p2[0,0], r[1,0], p2[2,0], np.linalg.norm(p))
			except ValueError:
				p2=np.matrix([[0],[0],[MAPSCALE*5/10]])
				r=np.matrix([[0],[0],[0]])
				pass

			X=(99-10*p2[0,0]/MAPSCALE)
			Y=(68+10*p2[2,0]/MAPSCALE)
			ANG=-90-180*r[1,0]/3.141519
			print("X,Y,ANG",X,Y,ANG)
			POINTS=[]
			POINTS.append([])
			POINTS[0]=[X,Y]
			POINTS.append([])
			POINTS[1]=[97,71]
			POINTS.append([])
			POINTS[2]=[99,71]
	
			#ejecutar_mov(Optimal_path,X,Y,ANG,Orientacion_final)
			ejecutar_mov_vertices(POINTS,X,Y,ANG,Orientacion_final)
			#print("X,Y,ANG::>",X,Y,ANG)



		sys.exit()





		speak.say("<speak>Me estoy dirigiendo a su destino. Por favor, acompáñeme</speak>")
		while speak.IsSpeaking():
			pass	
		
		#variable auxiliar para indicar fin del camino		
		final=len(Optimal_path)
		quiebros = []

		
		print("Las coordenadas iniciales son X={} Y={} ANG={}".format(X,Y,ANG))
		print("En milimetros son X = {} Y = {} ANG = {}".format(X*MAPSCALE,Y*MAPSCALE,ANG))
		
		#Movimiento INICIAL
		quiebros, direcciones = comprobarQuiebros()
		print(quiebros)
		print(direcciones)
		#print("Comprobacion de quiebros y direcciones:")
		#print("\tQuiebros:\n {}".format(quiebros))
		#print("\tDirecciones:\n {}".format(direcciones))

		#Si tenemos quiebros en el camino
		#if quiebros:
		#	print("Los quiebros se hacen en los puntos:")
		#	print(quiebros)
		#	print("Navegacion con quiebros en el camino no implementada todavía") 
		#Si no tenemos ningun quiebro		
		#else:
		#Y el camino es horizontal
		if direcciones[0]=='WE' or direcciones[0]=='EA':
			print("No ha habido que girar en el camino")
			#Distancia a recorrer:
			nodosRecorrer = abs(Optimal_path[0][0]-Optimal_path[-1][0])
			print("Nodos a recorrer = {}".format(nodosRecorrer))
			Xdestino = Optimal_path[-1][0]
			Ydestino = Optimal_path[-1][1]
			print("Destino es X= {}  Y={} ".format(Xdestino, Ydestino))
			avanzarRecto('inicial',nodosRecorrer, Xdestino, Ydestino)
		os.system(rutaAudios+"/llegado.mp3 &")
		time.sleep(2)
			
	 
	except Exception as inst:
		#Informe error
		print (inst)
		#Fin informorme
	finally:
		pygame.mixer.quit()
		#Cerrado y finalizacion
		ser.write('?'.encode('ascii'))
		time.sleep(0.5)
		ser.close()
		#Q.close()
			


if __name__ == '__main__':
	   main()
