import serial
import time

try:
	valor = []
	lectura = 0
	ser = serial.Serial('/dev/ttyAMA0',9600,timeout=3)
	print('Conectado')
	time.sleep(2)
	#Reset de la cuenta de ambos encoders
	ser.write('<'.encode('ascii'))
	time.sleep(5)
	#Comenzamos la funcion de calibracion (Ascii -> 2)
	print("Comenzando calibracion")
	ser.write(b'2')

	print("Esperamos 18 segundos")
	time.sleep(15)
	print("3 segundos")
	time.sleep(1)
	print("2 segundos")
	time.sleep(1)
	print("1 segundos")
	time.sleep(1)

	valor = ser.readline().decode('ascii')

	#Nos quitamos los caracteres que señalizan cada valor
	print("Datos de velocidad izquierda/derecha -)VIZ*VDER- final y")
	print(")VIZ*VDER")
	print(valor);

	valor = ser.readline().decode('ascii')
	print("lectura de los encoders izquierdo/derecho -!ENI*END-")
	print("!ENI*END")
	print(valor);
	#vIZQ, vDER = valor[1:-2].split('*')

	#print("\nVelocidad final de la rueda izquierda")
	#print(vIZQ)
	#print("\nVelocidad final de la rueda derecha")
	#print(vDER)


except Exception as inst:
	print(inst)

finally:
	ser.write('?'.encode('ascii'))
	time.sleep(0.3)
	ser.close()
