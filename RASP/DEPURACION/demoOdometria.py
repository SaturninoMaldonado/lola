# coding: utf-8

import zbar
import cv2
import numpy
import math

#Abrir cámara usb
cam=cv2.VideoCapture(0)
res, rgb=cam.read()
if not res:
    print("Error cámara usb.")
    exit()
 
#Capturamos tamaño de la imagen:   
h, w=rgb.shape[0:2]

#Parámetros internos de la cámara:
#Distancia focal (la definimos en función del ancho de la imagen):
f=0.89*w
#Coordenadas del punto principal:
x0=w/2
y0=h/2
#Matriz de parámetros internos:
K=numpy.matrix([
    [f, 0, x0],
    [0, f, y0],
    [0, 0,  1]], numpy.float32)

#Parámetros del código QR:
#Tamaño del cuadrado entre 2:
d=2.5 #cm
#Coordenadas de las cuatro esquinas del código QR:
#En el objeto real:
Co=numpy.array([
    [-d, -d, 0],
    [-d, +d, 0],
    [+d, +d, 0],
    [+d, -d, 0]], numpy.float32)
#En la imagen (se actualizan para cada imagen en la que se detecte el código):
Ci=numpy.zeros((4, 2), numpy.float32)

#Inicializar el detector de códigos QR
scanner=zbar.Scanner()

#Representación de resultados:
#Imagen donde pintar el resultado:
planoXZ=numpy.zeros((500, 500), numpy.uint8)
#Escala de conversión unidades 
escala=10.0
#Longitud del puntero de representación de la orientación.
puntero=100 #Píxeles.
#Imagen para representar la inclinación de la cámara.
planoAB=numpy.zeros((500, 500), numpy.uint8)

#Variables para filtrado de resultados.
t2=None
r2=None
alfa=1.0

#Bucle principal. Salimos cuando el usuario pulsa Esc.
while (True):
    res, rgb=cam.read()
    if not res:
        print("Error cámara usb.")
        exit()
    #Convertir imagen a escala de grises.
    grs=cv2.cvtColor(rgb, cv2.COLOR_BGR2GRAY)
    #Detectar código.
    codigos=scanner.scan(grs)
    #Poner a negro la imagen del resultado.
    planoXZ[:]=0x00
    planoAB[:]=0x00
    h=0
    while h<planoXZ.shape[1]:
        h+=50
        cv2.line(planoXZ, (0, h), (planoXZ.shape[0], h), 0x80, 1)
        cv2.line(planoXZ, (h, 0), (h, planoXZ.shape[1]), 0x80, 1)

    
    #Si hemos detectado un código, realizamos el proceso de localización.
    if len(codigos)>=1:
        Cp=codigos[0].position
        n=0
        for c in Cp:
            Ci[n, 0]=c[0]
            Ci[n, 1]=c[1]
            cv2.circle(rgb, c, 4, (0xFF, 0x00, 0x00), -1)
            n+=1
            cv2.putText(rgb, "%d" % n, c, cv2.FONT_HERSHEY_SIMPLEX, 1, (0x00, 0x00, 0xFF), 2)
        res, r1, t1=cv2.solvePnP(Co, Ci, K, None)
        print("Posicion:", t1.T)
        print("Orientacion:", r1.T)
        #Escalamos la medida para ajustarlo al espacio disponible en la
        #imagen de representación de resultados.
        t1*=escala
        try:
            t2=(1-alfa)*t2+alfa*t1
            r2=(1-alfa)*r2+alfa*r1
        except Exception:
            t2=t1
            r2=r1
        #Representación de resulados:
        #Pintar un punto representando la posición de la cámara.
        p1x=0
        p1y=planoXZ.shape[0]/2
        p1=( int(p1x), int(p1y) )
        cv2.circle(planoXZ, p1, 6, 0x80, -1)
        #######################################################################
        #######################################################################
        p1x+=t2[2, 0]
        p1y+=t2[0, 0]
        #######################################################################
        p1=( int(p1x), int(p1y) )
        cv2.circle(planoXZ, p1, 6, 0xFF, -1)
        #Pintar orientación de la cámara en el planoXZ horizontal.
        p2x=p1x
        p2y=p1y
        #######################################################################
        #######################################################################
        p2x-=puntero*math.cos(r2[1, 0])
        p2y-=puntero*math.sin(r2[1, 0])
        #######################################################################
        p2=( int(p2x), int(p2y) )
        cv2.line(planoXZ, p1, p2, 0xFF, 2)
        #Pintar inclinación de la cámara:
        #Elevación de la cámara:
        ev=planoAB.shape[0]/2
        #######################################################################
        #######################################################################
        ev+=planoAB.shape[0]/2*math.tan(r2[0])
        #######################################################################
        #Puntos extremos de la línea a pintar:
        p1lx=0
        p1ly=ev
        p2lx=planoAB.shape[1]
        p2ly=ev
        #######################################################################
        #######################################################################
        p1ly+=planoAB.shape[1]/2*math.tan(r2[2])
        p2ly-=planoAB.shape[1]/2*math.tan(r2[2])
        #######################################################################
        p1l=( int(p1lx), int(p1ly) )        
        p2l=( int(p2lx), int(p2ly) )        
        cv2.line(planoAB, p1l, p2l, 0xFF, 4)
        
    cv2.imshow("camara", rgb)
    cv2.imshow("pose", planoXZ)
    cv2.imshow("planoAB", planoAB)
    c=cv2.waitKey(5) & 0xFF
    if c==27:
        break

