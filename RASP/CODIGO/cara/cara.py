#!/usr/bin/python2
# -*- coding: utf-8 -*-

# Import statements
# system
import time as t
from threading import Thread
# Ui framework
import Tkinter as tk
# from tkinter import ttk
# image processing
from PIL import ImageTk

from polly_tts import *

class cara(tk.Frame):
    def __init__(self, lola):
        self.lola_obj = lola
        # Invocamos el constructor de la clase padre para construir la ventana de la aplicacion
        super().__init__(lola.root)
        self.window = lola.root
        lola.root.title("Despertador de Lola")
        lola.root.configure(width=800, height=480)

        # Marco de la aplicación tenga siempre el mismo tamaño que la ventana
        self.place(relwidth=1, relheight=1)  # tamaños relativos respecto al padre 0-1

        # Cuando se pulse boton izquierdo del raton sobre la ventana:
        self.canvas = tk.Canvas(self, width=800, height=480, background="white")
        self.canvas.grid(row=0, column=0)
        self.canvas.bind("<Button-1>", self.callback)

        # images
        self.blink = []
        self.blinkTimes = 0
        self.tired = []
        self.blink.append(ImageTk.PhotoImage(file="cara/CARA_00.png"))
        self.blink.append(ImageTk.PhotoImage(file="cara/CARA_01.png"))
        self.blink.append(ImageTk.PhotoImage(file="cara/CARA_02.png"))
        self.blink.append(ImageTk.PhotoImage(file="cara/CARA_03.png"))
        self.blink.append(ImageTk.PhotoImage(file="cara/CARA_04.png"))
        self.blink.append(ImageTk.PhotoImage(file="cara/CARA_03.png"))
        self.blink.append(ImageTk.PhotoImage(file="cara/CARA_02.png"))
        self.blink.append(ImageTk.PhotoImage(file="cara/CARA_01.png"))
        self.blink.append(ImageTk.PhotoImage(file="cara/CARA_00.png"))

        self.tired.append(ImageTk.PhotoImage(file="cara/CARA_10.png"))
        self.yawn = ImageTk.PhotoImage(file="cara/CARA_20.png")
        self.my_image_number = 0

        # set first image on canvas
        self.image_on_canvas = self.canvas.create_image(0, 0, anchor="nw", image=self.blink[self.my_image_number])

        self.createWidgets()
        self.out = 0

        # button to change image
        # self.button = tk.Button(self, text="Change", command=self.changeImages)
        # self.button.place(relx=0.0, rely=0.1)
        self.changeImages()

    def callback(self, event):
        self.lola_obj.window.lift()
        self.lola_obj.synthesizer.say(u"<speak>¿Que podemos hacer hoy?</speak>")
        t.sleep(0.2)
        self.window.lower()
        self.lola_obj.start_timeout(self.lola_obj)

    def createWidgets(self):
        self.quitButton = tk.Button(self, text='Quit', command=self.kill)
        self.quitButton.place(relx=0.0, rely=0.0)

    def changeImages(self):

        while self.my_image_number <= len(self.blink) and not self.out:
            # next image
            self.my_image_number += 1

            if self.blinkTimes == 5:
                self.canvas.itemconfig(self.image_on_canvas, image=self.yawn)
                self.update()
                t.sleep(1)
                self.blinkTimes = 0
                self.canvas.itemconfig(self.image_on_canvas, image=self.blink[self.my_image_number])
                self.update()
                t.sleep(1)

            # return to first image
            if self.my_image_number == len(self.blink):
                self.my_image_number = 0
                self.blinkTimes += 1
                t.sleep(4)

            # change image
            self.canvas.itemconfig(self.image_on_canvas, image=self.blink[self.my_image_number])
            self.update()

            t.sleep(0.02)

    def kill(self):
        self.out = 1
        quit()


if __name__ == '__main__':
    main_window = tk.Tk()

    app = cara(main_window)
    app.mainloop()
